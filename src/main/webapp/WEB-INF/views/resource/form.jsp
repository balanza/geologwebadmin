<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>POI form</title>

</head>
<body>
	<form:form action="" method="post" modelAttribute="resource"
		enctype="multipart/form-data">

		<div>
			<label for="poi">poi</label> <select name="poiId">
				<option value="0"></option>
				<c:forEach var="poi" items="${model.pois}">


					<option value="${poi.id}"
						<c:if test="${model.resource.poi.id == poi.id}"> selected="true" </c:if>>${poi.name}</option>
				</c:forEach>
			</select>
		</div>


		<div>
			<label for="description">description</label>
			<textarea name="description">${model.resource.description}</textarea>
		</div>


		<div id="uploader-field">
			<label for="upl">file</label> <input type="file" name="upl" />
		</div>
${model.resource.url} -
		<div id="file-field">
			<label for="file">file</label> <img alt=""
				src="/web/${model.resource.url}"> <input name="url"
				type="text" enabled="false" value="${model.resource.url}" /> <a
				class="action" href="remove-file">remove</a>
		</div>



		<div>
			<input type="submit" value="Save" />
		</div>
	</form:form>

	<jsp:include page="/WEB-INF/views/commons/scripts.jsp"></jsp:include>
	<script>
		$(function() {

			if ($('[name="url"]').val() == '') {

				$('#uploader-field').show();
				$('#file-field').hide();

			} else {

				$('#uploader-field').hide();
				$('#file-field').show();

				$('#file-field').find('a.action').click(function(e) {
					e.preventDefault();

					$('#uploader-field').show();
					$('#file-field').hide();

				});

			}

		});
	</script>

</body>
</html>