<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Poi | list</title>
<jsp:include page="/WEB-INF/views/commons/styles.jsp"></jsp:include>
<style type="text/css" title="currentStyle">


#wrapper{width: 90%; margin: 0 auto; margin-top: 10px;}
h1, #list, #map {padding: 0 20px;}
.dataTables_filter, .dataTables_length, .dataTables_info, .dataTables_paginate {width: 49%; float: left;}

.dataTables_paginate a{
	display: inline-block;
	padding: 0 20px;
	cursor: pointer;
}
#map{margin-top: 44px;}

</style>
</head>
<body>

	<div id="wrapper">

	<header>
	

<jsp:include page="/WEB-INF/views/commons/navbar.jsp"></jsp:include>

	<h1>List of all POI</h1>
	</header>

	<div  class="row-fluid container">
		<section id="list" class="span8">
		<table id="data" style="display: none"
			class="table table-bordered table-striped table-hover">
			<thead>
				<th>id</th>
				<th>name</th>
				<th>category</th>
				<th>description</th>
				<th>approved</th>
				<th>
				</td>
				<th></th>
			</thead>
			<tbody>
				<c:forEach var="poi" items="${model.pois}">
					<tr>
						<td>${poi.id}</td>
						<td>${poi.name}</td>
						<td><a href="/web/admin/category/edit/${poi.category.id}"
							title="go editing this category">${poi.category.name}</a></td>
						<td>${poi.description}</td>
						<td><input type="checkbox" name="valid" value="${poi.id}"
							<c:if test="${poi.valid}" > checked </c:if> /></td>
						<td><a class="action edit" href="edit/${poi.id}">edit</a></td>
						<td><a class="action delete" href="delete/${poi.id}">delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</section>
		<aside id="map" class="span4">
		<div id="map-container" style="height: 500px; max-width: 100%"></div>
		</aside>
	</div>
</div>

	<jsp:include page="/WEB-INF/views/commons/scripts.jsp"></jsp:include>
	<script src="/web/assets/scripts/jquery.dataTables.js"></script>

	<script>
		$(function() {

			//inizializzazione tabella
			$("table#data").dataTable();
			$("table#data").show();
			
			
			//validazione
			$('input[name="valid"]').change(function(e){
				var $this = $(this);
				var prev = $this.is(':checked');
				var next = !prev;
				var poi = $this.val();
				
				$.ajax({
					async : true,
					dataType : "json",
					url : '/web/admin/poi/edit/' + poi + '/validate/' + next,
					success : function(data) {
						$this.attr('checked', next);						
					},
					error: function(data){}
				
				});				
			});

			//inizializzazione mappa
			$('#map-container').height($('#map-container').parents('.container').find('table#data').height());
			var map = (function() {

				var marker = null; //marker corrente 
				var mapArea = new GMaps({ //area della mappa
					div : '#map-container',
					lng : 12.460728,
					lat : 41.90151,
					zoom : 11

				});

				$.ajax({
					async : true,
					dataType : "json",
					url : '/web/admin/poi/list?json',
					success : function(data) {
						var m;
						for ( var i = 0; m = data[i]; i++) {
							var w = (function(poi) {

								var $b = $('<div>');

								var p = $('<a>').attr('href',
										'/web/admin/poi/edit/' + poi.id).attr(
										'title', 'go edit this poi').appendTo(
										$b).wrap('<h5>').html(poi.name);

								$('<a>').attr(
										'href',
										'/web/admin/category/edit/'
												+ poi.category_id).attr(
										'title', 'go edit this category')
										.appendTo($b).wrap('<h6>').html(
												poi.category_name);

								$('<p>').appendTo($b).html(poi.description);
								//console.log($b.html());
								return $b.html();
							})(m);
							mapArea.addMarker({
								lat : m.latitude,
								lng : m.longitude,
								title : m.name,
								draggable : false,
								infoWindow : {
									content : w
								}
							});
						}
						;
					},
					error : console.log
				});

				return mapArea;

			})();

		});
	</script>

</body>
</html>