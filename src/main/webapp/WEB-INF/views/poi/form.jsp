<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>POI form</title>
<jsp:include page="/WEB-INF/views/commons/styles.jsp"></jsp:include>
<style>
#wrapper{width: 90%; margin: 0 auto; margin-top: 10px;}
h1, #data, #map {padding: 0 20px;}
#map-container{height: 500px;}

form label {display: inline-block; width: 140px;}

</style>

</head>
<body>
	<div id="wrapper">

		<header> <jsp:include
			page="/WEB-INF/views/commons/navbar.jsp"></jsp:include>

		<h1>List of all POI</h1>
		</header>

		<div class="row-fluid container">
			<section id="data" class="span6"> <form:form action="new"
				method="post" modelAttribute="poi">

				<div class="control-group">
					<label class="control-label" for="name">name</label> <input name="name" type="text"
						value="${model.poi.name}" />
				</div>
				<div class="control-group">
					<label class="control-label" for="description">description</label>
					<textarea name="description">${model.poi.description}</textarea>
				</div>
				<div class="control-group">
					<label class="control-label" for="category">category</label> <select name="categoryId">
						<option value="0"></option>
						<c:forEach var="category" items="${model.categories}">
							<option value="${category.id}">${category.name}</option>
						</c:forEach>
					</select>
				</div>

				<div class="control-group">
					<label class="control-label" for="latitude">longitude</label> <input name="latitude"
						enabled="false" type="text" value="${model.poi.latitude}" />
				</div>
				<div class="control-group">
					<label class="control-label" for="longitude">longitude</label> <input name="longitude"
						enabled="false" type="text" value="${model.poi.longitude}" />
				</div>




				<div>
					<input type="submit" value="Save" />
				</div>
			</form:form> </section>

			<aside id="map" class="span6">
			<div id="map-container"></div>
			</aside>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/commons/scripts.jsp"></jsp:include>

	
	<c:if test="${model.poi.id == 0}">

	<script>
		$(function() {

			var form = (function() {

				return {

					updateLatLng : function(lat, lng) {
						$('input[name="latitude"]').val(lat);
						$('input[name="longitude"]').val(lng);
					}

				};

			})();

			$('#map-container').height($('#map-container').parents('.container').height());
			var map = (function() {

			
				var marker = null; //marker corrente 
				var mapArea = new GMaps({ //area della mappa
					div : '#map-container',
					lng : 12.460728,
					lat : 41.90151,
					zoom : 11,
					click : function(e) {
						if (!marker) {
							marker = mapArea.addMarker({
								lat : e.latLng.hb,
								lng : e.latLng.ib,
								title : 'Poi',
								details : {
									database_id : 42,
									author : '@balanza'
								},
								draggable : true,
								dragend : function() {
									var p = marker.getPosition();
									form.updateLatLng(p.hb, p.ib);
								}
							});
							//update dei campi della form
							var p = marker.getPosition();
							form.updateLatLng(p.hb, p.ib);
						}

					}
				});
			})();

		});
	</script>
	
	</c:if>
	
	<c:if test="${model.poi.id != 0}">

	<script>
		$(function() {

			var form = (function() {

				return {

					updateLatLng : function(lat, lng) {
						$('input[name="latitude"]').val(lat);
						$('input[name="longitude"]').val(lng);
					}

				};

			})();

			$('#map-container').height($('#map-container').parents('.container').find('table#data').height());
			var map = (function() {

			
				var marker = {
						lat : '${model.poi.latitude}',
						lng : '${model.poi.longitude}',
						title : '${model.poi.name}',
						draggable : true,
						dragend : function() {
							var p = marker.getPosition();
							form.updateLatLng(p.hb, p.ib);
						}
					};
				var mapArea = new GMaps({ //area della mappa
					div : '#map-container',
					lng : 12.460728,
					lat : 41.90151,
					zoom : 11
				});
				
				mapArea.addMarker(marker);
			})();

		});
	</script>
	
	</c:if>
	
	

</body>
</html>