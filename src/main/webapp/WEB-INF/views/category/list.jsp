<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Category list</title>
</head>
<body>
	
	<table>
		<thead>		
			<td>id</td>
			<td>icon</td>
			<td>name</td>
			<td>description</td>
			<td></td>			
			<td></td>
		</thead>
		<tbody>
			<c:forEach var="category" items="${categories}">
				<tr>
					<td>${category.id}</td>
					<td><img src="/web/${category.icon}" /></td>
					<td>${category.name}</td>
					<td>${category.description}</td>
					<td><a href="edit/${category.id}">edit</a></td>
					<td><a href="delete/${category.id}">delete</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>


</body>
</html>