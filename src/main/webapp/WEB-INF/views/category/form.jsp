<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Category form</title>
</head>
<body>
	<form:form action="" method="post" modelAttribute="category" enctype="multipart/form-data">
		
		<div>
		
			<label for="name">name</label>
			<input name="name" type="text" value="${category.name}"/>
		
		</div>
		
		<div>
			<label for="description">description</label>
			<textarea name="description">${category.description}</textarea>
		</div>
		
			
		<div id="uploader-field">
			<label for="icon_upl">icon</label>
			<input type="file" name="icon_upl" />
		</div>
		
		<div id="icon-field">
			<label for="icon">icon</label>
			<img alt="" src="/web/${category.icon}">
			<input name="icon" type="text" enabled="false" value="${category.icon}" />
			<a class="action" href="remove-icon" >remove</a>
		</div>
		
		
		
		<div>
			<input type="submit" value="Save" />
		</div>
	</form:form>
	
	<jsp:include page="/WEB-INF/views/commons/scripts.jsp"></jsp:include>
	<script>
		
	$(function(){
		
		if($('[name="icon"]').val() == ''){
			
			$('#uploader-field').show();
			$('#icon-field').hide();
			
		} else {
			

			$('#uploader-field').hide();
			$('#icon-field').show();
			
			$('#icon-field').find('a.action').click(function(e){
				e.preventDefault();
				

				$('#uploader-field').show();
				$('#icon-field').hide();
				
			});
			
		}
		
	});
	
	</script>
</body>
</html>