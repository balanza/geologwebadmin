
 <!-- Navbar
    ================================================== -->
 <div class="navbar">
   <div class="navbar-inner">
     <div class="container">
       <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
       </a>
       <a class="brand" href="/web/">Geolog</a>
       <div class="nav-collapse collapse" id="main-menu">
        <ul class="nav" >
          <li><a  href="/web/#gallery">Home</a></li>
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown">Admin <b class="caret"></b></a>
            <ul class="dropdown-menu" id="swatch-menu">          
              <li><a href="/web/admin/poi/list">browse POIs</a></li>
              <li><a href="/web/admin/poi/new">add new POI</a></li>
              <li class="divider"></li>         
              <li><a href="/web/admin/category/list">browse Categories</a></li>
              <li><a href="/web/admin/category/new">add new Category</a></li>
            </ul>
          </li>
          
        </ul>
        <ul class="nav pull-right" id="main-menu-right">
          <li class="dropdown" id="preview-menu">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Source <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a target="_blank" href="#">App</a></li>
              <li><a target="_blank" href="#">Back Office</a></li>
            </ul>
          </li>
          <li><a target="_blank" href="#">API</a></li>
          <li><a target="_blank" href="#">Download App</a></li>  </ul>
       </div>
     </div>
   </div>
  </div>