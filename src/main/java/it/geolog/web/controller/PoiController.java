package it.geolog.web.controller;

import it.geolog.web.dao.CategoryDAO;
import it.geolog.web.dao.PoiDAO;
import it.geolog.web.domain.Category;
import it.geolog.web.domain.Poi;
import it.geolog.web.domain.Resource;
import it.geolog.web.utils.ApplicationContextSingleton;
import it.geolog.web.utils.SpringApplicationContext;

import java.io.File;
import java.net.BindException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import com.google.gson.JsonObject;

/*import de.micromata.opengis.kml.v_2_2_0.Coordinate;
 import de.micromata.opengis.kml.v_2_2_0.Document;
 import de.micromata.opengis.kml.v_2_2_0.Feature;
 import de.micromata.opengis.kml.v_2_2_0.Folder;
 import de.micromata.opengis.kml.v_2_2_0.Kml;
 import de.micromata.opengis.kml.v_2_2_0.Placemark;
 import de.micromata.opengis.kml.v_2_2_0.Point; */

@Controller
@RequestMapping(value = "/poi")
public class PoiController {

	private static PoiDAO dao = SpringApplicationContext.getBean(PoiDAO.class);
	private static CategoryDAO cdao = SpringApplicationContext
			.getBean(CategoryDAO.class);

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView read(@PathVariable("id") int id) {

		Poi poi = dao.find(id);
		List<Category> categories = cdao.findAll();

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("poi", poi);
		model.put("categories", categories);

		return new ModelAndView("/poi/form", "model", model);
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public ModelAndView update(@PathVariable("id") int id,
			@ModelAttribute("Poi") Poi poi) {

		dao.save(poi);

		List<Category> categories = cdao.findAll();

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("poi", poi);
		model.put("categories", categories);

		return new ModelAndView("/poi/form", "model", model);
	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public ModelAndView create() {

		List<Category> categories = cdao.findAll();

		Map<String, Object> model = new HashMap<String, Object>();

		model.put("categories", categories);

		return new ModelAndView("/poi/form", "model", model);
	}

	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public View create(@ModelAttribute("Poi") Poi poi, int categoryId) {

		Category category = cdao.find(categoryId);

		poi.setCategory(category);

		dao.save(poi);

		return new RedirectView("edit/" + poi.getId());
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public View delete(@PathVariable("id") int id) {

		dao.delete(id);

		return new RedirectView("../list");
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request,
			HttpServletResponse response, BindException errors) {

		// lettura parametri qs
		/*
		 * String key = request.getParameter("key"); int categoryId =
		 * Integer.parseInt(request.getParameter("categoyId")); int approvation
		 * = Integer.parseInt(request.getParameter("approvation"));
		 */

		Map<String, Object> model = new HashMap<String, Object>();

		model.put("pois", dao.findAll(false));

		return new ModelAndView("/poi/list", "model", model);
	}
	
	
	
	@RequestMapping(value = "/edit/{id}/validate/{valid}", method = RequestMethod.GET)
	public @ResponseBody
	void validate(@PathVariable("id") int id, @PathVariable("valid") Boolean valid) {
		Poi poi = dao.find(id);
		poi.setValid(valid);
		dao.save(poi);
	}
	

	@RequestMapping(value = "/list", params = "json", method = RequestMethod.GET)
	public @ResponseBody
	String export() {

		List<Poi> pois = dao.findAll(false);
		JSONArray jarray = new JSONArray();

		for (Poi poi : pois) {

			JSONObject o = new JSONObject();

			o.put("id", poi.getId());
			o.put("name", poi.getName());
			o.put("description", poi.getDescription());
			o.put("latitude", poi.getLatitude());
			o.put("longitude", poi.getLongitude());
			o.put("category_id", poi.getCategory().getId());
			o.put("category_name", poi.getCategory().getName());

			JSONArray res = new JSONArray();

			for (Resource r : poi.getResources()) {

				JSONObject jr = new JSONObject();
				jr.put("id", r.getId());
				jr.put("url", r.getUrl());
				jr.put("resourceType", r.getResourceType());

				res.add(jr);
			}

			o.put("resources", res);

			jarray.add(o);
		}

		return jarray.toJSONString();

	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public View root() {
		return new RedirectView("list");
	}

	/*
	 * @RequestMapping(value = "/import", method = RequestMethod.GET) public
	 * View bulk() {
	 * 
	 * Category cat = SpringApplicationContext.getBean(Category.class);
	 * cat.setName("caegoria di test"); cat.setDescription("");
	 * 
	 * cdao.save(cat);
	 * 
	 * 
	 * System.out.println("This is KML test"); final Kml kml = Kml.unmarshal(new
	 * File("/home/emanuele/Downloads/Negozi.kml")); final Document document =
	 * (Document) kml.getFeature(); System.out.println( document.getName());
	 * List<Feature> t = document.getFeature(); for(Object o : t){ Folder f =
	 * (Folder)o; List<Feature> tg = f.getFeature(); for(Object ftg : tg){
	 * Placemark pm = (Placemark) ftg; System.out.println(pm.getName()); Point
	 * point = (Point) pm.getGeometry(); List<Coordinate> coordinates =
	 * point.getCoordinates(); for (Coordinate coordinate : coordinates) {
	 * System.out.println(coordinate.getLatitude());
	 * System.out.println(coordinate.getLongitude());
	 * System.out.println(coordinate.getAltitude());
	 * 
	 * Poi poi = SpringApplicationContext.getBean(Poi.class);
	 * poi.setName(pm.getName()); poi.setDescription(pm.getDescription());
	 * poi.setCategory(cat); poi.setLatitude((float)coordinate.getLatitude());
	 * poi.setLongitude((float)coordinate.getLongitude());
	 * 
	 * dao.save(poi);
	 * 
	 * 
	 * } } } return new RedirectView( "list"); }
	 */

}
