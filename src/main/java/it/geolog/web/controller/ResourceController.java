package it.geolog.web.controller;

import it.geolog.web.dao.*;
import it.geolog.web.domain.*;
import it.geolog.web.utils.ApplicationContextSingleton;
import it.geolog.web.utils.SpringApplicationContext;
import it.geolog.web.utils.uploader.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Controller che gestisce le operazioni sulle risorse.
 * 
 * @author Emanuele De Cupis
 */
@Controller
@RequestMapping(value = "/resource")
public class ResourceController {

	private static PoiDAO pdao = SpringApplicationContext.getBean(PoiDAO.class);
	private static ResourceDAO rdao = SpringApplicationContext
			.getBean(ResourceDAO.class);

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public ModelAndView create() {
		Map<String, Object> model = new HashMap<String, Object>();

     	model.put("pois", pdao.findAll(false));
		return new ModelAndView("/resource/form", "model", model);
	}

	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public View create(@ModelAttribute("Resource") Resource resource, MultipartFile upl, int poiId) {
		
		uploadFile(upl, resource);
		resource.setPoi(pdao.find(poiId));

		rdao.save(resource);

		return new RedirectView("edit/" + resource.getId());
	
	}
	
	 @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	    public ModelAndView read(@PathVariable("id") int id) {
	    	
	     	
	     	
	     	Map<String, Object> model = new HashMap<String, Object>();
	     	model.put("pois", pdao.findAll(false));
	     	model.put("resource", rdao.find(id));
	        
	        return new ModelAndView("/resource/form" ,  "model", model);
	    }
	   
	    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	    public RedirectView update(@PathVariable("id") int id, @ModelAttribute("Resource") Resource resource, MultipartFile file, int poiId) {
	    	
	    	
	    	uploadFile(file, resource);
			resource.setPoi(pdao.find(poiId));

			rdao.save(resource);

			return new RedirectView("/resource/edit/" + resource.getId());
	    	

	    }
	    
	    

	    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	    public View delete(@PathVariable("id") int id) {
	    	
	    	rdao.delete(id);    	
	    	        
	        return new RedirectView( "../list");
	        
	        
	    }
	    
	    @RequestMapping(value = "/list", method = RequestMethod.GET)
	    public ModelAndView list() {
	        
	    	
	       	
	        return new ModelAndView("/resource/list", "resources", rdao.findAll());
	    }
	    
	    @RequestMapping(value = "/", method = RequestMethod.GET)
	    public View root() {
	    	 return new RedirectView( "list");
	    }
	    
	     
	

    private void uploadFile(MultipartFile upl, Resource res){
    	
    	if(upl != null && !upl.isEmpty()){
    		try {
    			String url;
				AbstractResourceUploader uploader = ResourceUploaderFactory.create(upl.getContentType());
				url= uploader.upload(upl.getBytes());
				res.setUrl(url);
				res.setResourceType(upl.getContentType());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    	}
    }

}
