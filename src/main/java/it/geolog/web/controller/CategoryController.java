package it.geolog.web.controller;

import it.geolog.web.dao.*;
import it.geolog.web.domain.*;
import it.geolog.web.utils.ApplicationContextSingleton;
import it.geolog.web.utils.SpringApplicationContext;
import it.geolog.web.utils.uploader.*;

import java.util.HashMap;
import java.util.Map;



import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;




/**
 * Controller che gestisce le operazioni sulle categorie. 
 * 
 * @author Emanuele De Cupis
 */
@Controller
@RequestMapping(value = "/category")
public class CategoryController {
	

	private static CategoryDAO dao = SpringApplicationContext.getBean(CategoryDAO.class);
	
    /**
     * mostra la form di modifica della categoria specificata, se esistente
     * @param id id della categoria
     * @return ModelView 
     */
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView read(@PathVariable("id") int id) {
    	
     	Category cat = dao.find(id); 
        
        return new ModelAndView("/category/form" ,  "category", cat);
    }
   
    /** 
     * Cambia lo stato dei dati dell'applicazione, aggiornando la categoria specificata dall'id
     * @param id id dell categoria
     * @param category mappatura della categoria
     * @return 
     */
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public ModelAndView update(@PathVariable("id") int id, @ModelAttribute("Category") Category category,  MultipartFile icon_upl) {
    	
    	uploadIcon(icon_upl, category);
    	
    	category.setId(id);
     	dao.save(category);    	
    	        
        return new ModelAndView("/category/form" , "category", category);
    }
     
        
    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public ModelAndView create() {
         
        return new ModelAndView("/category/form", "category", null);
    }
    
    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public View create(@ModelAttribute("Category") Category category, MultipartFile icon_upl) {
        
    	uploadIcon(icon_upl, category);
    	
    	dao.save(category);   
    	
        return new RedirectView( "edit/" + category.getId());
    }
    
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public View delete(@PathVariable("id") int id) {
    	
    	dao.delete(id);    	
    	        
        return new RedirectView( "../list");
        
        
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
        
    	
       	
        return new ModelAndView("/category/list", "categories", dao.findAll());
    }
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public View root() {
    	 return new RedirectView( "list");
    }
    
    
    private void uploadIcon(MultipartFile icon_upl, Category cat){
    	
    	if(icon_upl != null && !icon_upl.isEmpty()){
    		try {
    			String icon;
				AbstractResourceUploader uploader = ResourceUploaderFactory.create(icon_upl.getContentType());
				icon = uploader.upload(icon_upl.getBytes());
				cat.setIcon(icon);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    	}
    }
    
    
}
