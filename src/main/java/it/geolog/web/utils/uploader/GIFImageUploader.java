package it.geolog.web.utils.uploader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;


/**
 * Classe che implenta un uploader di immagini JPG
 * @author emanuele
 *
 */
public class GIFImageUploader extends ImageUploader {

	public GIFImageUploader(){
		super();
		setExtension("gif");		
	}

	/**
	 * @see AbstractResourceUploader#upload(byte[], String)
	 */
	@Override
	public String upload(byte[] data, String name) {
		String relUrl = getRelativeUrl(name);
		
		File imageFile = new File(  getStoragePath() + "/" + relUrl );

		try 
		{
			imageFile.createNewFile();
			
			FileOutputStream fos = new FileOutputStream( imageFile );
			Base64 b64 = new Base64();
			byte[] encodedImage;
			try {
			 encodedImage = (byte[]) b64.decode(  data );
			} catch(Exception ex){
				encodedImage = data;
			}

			fos.write( encodedImage );
			fos.close();
			return getBaseUrl() + "/" + relUrl;

		} 
		catch( FileNotFoundException e ) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		catch( IOException e ) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}

}
