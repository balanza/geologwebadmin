package it.geolog.web.utils.uploader;

import it.geolog.web.utils.SpringApplicationContext;

import java.util.UUID;

import org.springframework.web.multipart.MultipartFile;

/**
 * Classe che definisce un generico uploader di risorse 
 * @author emanuele de cupis
 *
 */
public abstract class AbstractResourceUploader {
	
	protected final static String HOME_DIR = System.getProperty("user.home"); //cartella locale della macchina
	
	protected final static String STORAGE_PATH = "/Documents/workspace-sts-3.1.0.RELEASE/GeologWebAdmin/src/main/webapp/resources"; //cartella radice per lo storage dellle risorse
	
	protected String extension; //file extension
	
	/**
	 * metodo getter per l'estensione del file
	 * @return l'estensione del file
	 */
	public String getExtension() {
		return extension;
	}
	
	/**
	 * metodo setter per l'estensione del file
	 * @param extension l'estensione del file
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * metodo getter per la location di salvataggio del file
	 * @return
	 */
	protected  String getStoragePath(){
		return HOME_DIR + STORAGE_PATH;
	}
	
	/**
	 * metodo che implementa la business logic specifica di upload della risorsa
	 * @param data la risorsa, in formato array di byte
	 * @param name nome della risorsa
	 * @return url della risorsa
	 */
	public abstract String upload(byte[] data, String name);
	
	/**
	 * metodo che implementa la business logic specifica di upload della risorsa. alla risorsa viene dato un nome randomico
	 * @param data la risorsa, in formato array di byte
	 * @return url della risorsa
	 */
	public String upload(byte[] data){
		return upload(data, UUID.randomUUID().toString());
	}
	

	
	
	/**
	 * metodo che formatta correttamente in nome del file associato alla risorsa, includendo l'estensione
	 * @param name nome della risorsa
	 * @return nome del file formattato
	 */
	protected  String formatFileName(String name) {
		return name + "." + getExtension();
	}
	
	/**
	 * metodo getter per la url base delle risorse
	 * @return la url base delle risorse
	 */
	protected String getBaseUrl(){
		return "/resources";		
	}
	
	
	
}
