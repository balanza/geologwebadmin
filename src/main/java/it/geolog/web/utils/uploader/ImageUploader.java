package it.geolog.web.utils.uploader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;

/**
 * Classe che definisce un generico uploade di risorse di tipo immagine
 * @author emanuele de cupis
 *
 */
public abstract class ImageUploader extends AbstractResourceUploader {
 
	
	/**
	 * ritorna la url relativa della risorsa
	 * @param name nome della risorsa
	 * @return url relativa
	 */
	protected final String getRelativeUrl(String name){
		return "/images/" + formatFileName(name);		
	}

	/**
	 * @see AbstractResourceUploader#upload()
	 */
	@Override
	public String upload(byte[] data, String name) {
		String relUrl = getRelativeUrl(name);
		
		String p = (getStoragePath()  + relUrl).replace("/", "\\");
		
		File imageFile = new File(p );

		try 
		{
			imageFile.createNewFile();
			
			FileOutputStream fos = new FileOutputStream( imageFile );
			Base64 b64 = new Base64();
			byte[] encodedImage;
			try {
			 encodedImage = (byte[]) b64.decode(  data );
			} catch(Exception ex){
				encodedImage = data;
			}

			fos.write( encodedImage );
			fos.close();
			return getBaseUrl() + "/" + relUrl;

		} 
		catch( FileNotFoundException e ) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		catch( IOException e ) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}

}
