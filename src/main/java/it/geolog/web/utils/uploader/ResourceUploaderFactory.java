package it.geolog.web.utils.uploader;

/**
 * Classe factory per gli uploader di risorsa. a seconda del tipo istanzia il
 * corretto uploader
 * 
 * @author emanuele
 * 
 */
public class ResourceUploaderFactory {

	/**
	 * metodo che definisce la business logic del factory
	 * 
	 * @param type
	 *            tipo della risorsa
	 * @return un'istanza dell'uploader corretto
	 * @throws Exception
	 */
	public static AbstractResourceUploader create(String type) throws Exception {

		/*
		 * switch(type){ case "image/png": return new PNGImageUploader(); case
		 * "image/jpg": case "image/jpeg": return new JPGImageUploader();
		 * default: throw new Exception("Uploader type not found"); }
		 */

		if (type.equals("image/png")) {
			return new PNGImageUploader();
		} else if (type.equals("image/jpg") || type.equals("image/jpeg")) {
			return new JPGImageUploader();
		} else if (type.equals("image/gif")) {
			return new GIFImageUploader();
		} else {
			throw new Exception("Uploader type not found");
		}

	}

	/**
	 * metodo che, data l'estensione di un file, ne determina il mime type
	 * 
	 * @param ext
	 *            estensione
	 * @return mime type
	 */
	public static String extensionToType(String ext) {

		if (ext == "jpg") {
			return "image/jpg";
		} else if (ext == "png") {

			return "image/png";

		}
		return ext;

	}

}
