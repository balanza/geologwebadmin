package it.geolog.web.utils.uploader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;

/**
 * Classe che implenta un uploader di immagini JPG
 * @author emanuele
 *
 */
public class JPGImageUploader extends ImageUploader {

	public JPGImageUploader(){
		super();
		setExtension("jpg");		
	}

	

}
