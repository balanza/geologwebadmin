package it.geolog.web.utils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ApplicationContextSingleton {

	static private ApplicationContextAware instance = new SpringApplicationContext(); //new ClassPathXmlApplicationContext(new String[] {"/misc/beans/applicationContext.xml"});;
	
	private ApplicationContextSingleton(){}
	
	
	public static ApplicationContextAware getIntance(){
					
		return instance;

	}
	

}
