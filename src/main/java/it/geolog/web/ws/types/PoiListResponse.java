package it.geolog.web.ws.types;

import it.geolog.web.domain.Poi;
import it.geolog.web.domain.Resource;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


public class PoiListResponse extends BaseResponse {

	private List<Poi> pois; //lista di poi da ritornare
	
	/**
	 * metodo getter per la lista di poi
	 * @return la lista di poi
	 */
	public List<Poi> getPois() {
		return pois;
	}
	
	/**
	 * metodo setter per la lista di poi
	 * @param pois la lista di poi
	 */
	public void setPois(List<Poi> pois) {
		this.pois = pois;
	}
	
	/**
	 * legge il numero di elementi contenuto nella lista
	 * @return il numero di elementi contenuto nella lista
	 */
	public int getCount() {
		return getPois().size();
	}


	/**
	 * Implementa la logica di serializzazione
	 * @return la risposta seria�lizzata in un oggetto JSON
	 */
	@Override
	public String serialize() {
				
		JSONObject result = new JSONObject();
		
		JSONArray jarray = new JSONArray();
		
		if (getPois() != null){
			

			for( Poi poi : getPois()){
				
				JSONObject o = new JSONObject();

				o.put("id", poi.getId());
				o.put("name", poi.getName());
				o.put("description", poi.getDescription());
				o.put("latitude", poi.getLatitude());
				o.put("longitude", poi.getLongitude());
				o.put("category_id", poi.getCategory().getId());
				
				
				JSONArray res = new JSONArray();
				
				for(Resource r: poi.getResources()){
					
					JSONObject jr = new JSONObject();
					jr.put("id", r.getId());
					jr.put("url", r.getUrl());
					jr.put("resourceType", r.getResourceType());
					
					res.add(jr);
				}
				
				o.put("resources", res);
				
				
				jarray.add(o);
			}
			result.put("count", pois.size());
		}
		
		result.put("status", getStatus());
		result.put("message", getMessage());
		result.put("pois", jarray);
		
		return  result.toJSONString();
	}
	
	

}
