package it.geolog.web.ws.types;

import it.geolog.web.domain.Category;
import it.geolog.web.domain.Poi;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Implementa le business logic per la serializzazione di una risposta del webservice implicante una lista di categorie
 * @author emanuele
 *
 */
public class CategoryListResponse extends BaseResponse {

	private List<Category> categories; //lista di categorie da ritornare


	/**
	 * legge il numero di elementi contenuto nella lista
	 * @return il numero di elementi contenuto nella lista
	 */
	public int getCount() {
		return getCategories().size();
	}


	/**
	 * metodo getter per la lista di categorie
	 * @return la lista di categorie
	 */
	public List<Category> getCategories() {
		return categories;
	}

	/**
	 * metodo setter per la lista di categorie
	 * @param categories la lista di categorie
	 */
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}


	/**
	 * Implementa la logica di serializzazione
	 * @return la risposta seria�lizzata in un oggetto JSON
	 */
	@Override
	public String serialize() {
		
		JSONObject result = new JSONObject();
		JSONArray jarray = new JSONArray();

		for( Category cat : getCategories()){

			JSONObject o = new JSONObject();
			o.put("id", cat.getId());
			o.put("name", cat.getName());
			o.put("description", cat.getDescription());
			o.put("icon", cat.getIcon());

			jarray.add(o);
		}

		result.put("status",200);
		result.put("message", getMessage());
		result.put("count", getCategories().size());
		result.put("categories", jarray);

		return  result.toJSONString();
	}


}
