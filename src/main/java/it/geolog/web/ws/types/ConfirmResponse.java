package it.geolog.web.ws.types;

import it.geolog.web.domain.Poi;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Classe che implemnta una risposta del webservice di sola conferma dell'operazione
 * @author emanuele
 *
 */
public class ConfirmResponse extends BaseResponse {

	String result; //risultato dell'uperazione
	
	/**
	 * metodo getter per il risultato dell'operazione
	 * @return il risultato dell'operazione
	 */
	public String getResult() {
		return result;
	}

	/**
	 * metodo setter per il risultato dell'operazione
	 * @param result il risultato dell'operazione
	 */
	public void setResult(String result) {
		this.result = result;
	}

	
	/**
	 * implementa la logica di serializzazione
	 * @return la risposta serializzata
	 */
	@Override
	public String serialize() {
		JSONObject result = new JSONObject();
		
		
		
		result.put("status", getStatus());
		result.put("message", getMessage());
		result.put("result", getResult());
		
		return  result.toJSONString();
	}

}
