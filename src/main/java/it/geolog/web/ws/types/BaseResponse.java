package it.geolog.web.ws.types;

import java.io.Serializable;

/**
 * Classe base per le risposte fornite dai metodi del web service 
 * @author emanuele
 *
 */
public abstract class BaseResponse implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private int status; //stato della risposta 
	private String message; //messaggio della risposta
	
	/**
	 * metodo getter per lo stato della risposta
	 * @return lo stato della risposta
	 */
	public int getStatus() {
		return status;
	}
	
	/**
	 * metodo setter per lo stato della risposta
	 * @param status lo stato della risposta
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	/**
	 * metodo getter per il messaggio della risposta
	 * @return il messaggio della risposta
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * metodo setter per il messaggio della risposta
	 * @param message il messaggio della risposta
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	/**
	 * Esegue la serializzazione dell'oggetto
	 * @return la risposta serializzata
	 */
	public abstract String serialize();
	
	

}
