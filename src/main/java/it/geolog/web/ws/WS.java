package it.geolog.web.ws;


import it.geolog.web.dao.*;
import it.geolog.web.domain.*;
import it.geolog.web.utils.*;
import it.geolog.web.ws.types.*;
import it.geolog.web.utils.uploader.*;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.security.auth.callback.ConfirmationCallback;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.context.ApplicationContext;


import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * Classe che implementa le funzionalitą del web sevice da esporre alle applicazioni client
 * @author emanuele
 *
 */
public class WS implements IWS  {



	
	/*public String findNearby(double latitude, double longitude, int category_id) {

		PoiListResponse result = new PoiListResponse();

		try{
		

			CategoryDAO cdao = SpringApplicationContext.getBean(CategoryDAO.class);
			Category cat = cdao.find(category_id);
			List<Category> l = new ArrayList<Category>();
			l.add(cat);

			PoiDAO pdao = SpringApplicationContext.getBean(PoiDAO.class);
			List<Poi> pois = pdao.findAll(latitude, longitude, l);

			//if(pois == null) throw new Exception("Collezione di poi nulla");

			result.setCount(pois.size());
			result.setPois(pois);


			result.setStatus(200);
			result.setMessage("OK");	

		} catch(Exception e){
			result.setStatus(500);
			result.setMessage(e.getMessage());			
		}


		return result.serialize();



	}*/

	
	/**
	 * @see IWS#findNearby(double, double, String)
	 */	
	public String findNearby(double latitude, double longitude, String categories){

		PoiListResponse result = new PoiListResponse();

		try{

			CategoryDAO cdao = SpringApplicationContext.getBean(CategoryDAO.class);

			String[] cts = categories.split(",");
			int[] ids = new int[cts.length];

			for (int i = 0; i < cts.length; i++) {
				try {
					ids[i] = Integer.parseInt(cts[i]);
				} catch (NumberFormatException nfe) {};
			}

			List<Category> l = cdao.findAll(ids);

			PoiDAO pdao = SpringApplicationContext.getBean(PoiDAO.class);
			List<Poi> pois = pdao.findAll(latitude, longitude, l);

			//if(pois == null) throw new Exception("Collezione di poi nulla");
	
			
			result.setPois(pois);


			result.setStatus(200);
			result.setMessage("OK");	

		} catch(Exception e){
			result.setStatus(500);
			result.setMessage(e.getMessage());			
		}


		return result.serialize();

	}


	/**
	 * @see IWS#listCategories()
	 */
	@Override
	public String listCategories() {
		
		CategoryDAO cdao = SpringApplicationContext.getBean(CategoryDAO.class);

		CategoryListResponse clr = new CategoryListResponse();
		clr.setCategories(cdao.findAll());

		return clr.serialize();		

	}


	/**
	 * @see IWS#reportPoi(int, String, String)
	 */
	public String reportPoi(int poi_id, String msg, String user){
		//recupera poi

		PoiDAO pdao = SpringApplicationContext.getBean(PoiDAO.class);
		Poi poi = pdao.find(poi_id); 

		//crea nuovo report
		Report report = new Report();
		report.setMessage(msg);
		report.setUser(user);
		report.setPoi(poi);

		//salva il poi
		//	poi.getReports().add(report);
		ReportDAO rdao = SpringApplicationContext.getBean(ReportDAO.class);
		rdao.save(report);

		//ritorna conferma
		ConfirmResponse cr = new ConfirmResponse();
		cr.setMessage("OK");
		cr.setStatus(200);

		return cr.serialize();


	}


	/**
	 * @see IWS#addPoi(String, String)
	 */
	public String addPoi(String poi, String user){ 

		//deserializza il poi
try{

		JsonObject jo = new JsonParser().parse(poi).getAsJsonObject();

		PoiSuggestion  ps = new PoiSuggestion();
		ps.setName(jo.get("name").getAsString());
		ps.setDescription(jo.get("description").getAsString());
		ps.setLatitude(jo.get("latitude").getAsFloat());
		ps.setLongitude(jo.get("longitude").getAsFloat());

		/*for(JsonElement je : jo.get("resources").getAsJsonArray()){
			JsonObject jres = je.getAsJsonObject();
			try {
				AbstractResourceUploader uploader = ResourceUploaderFactory.create(jres.get("type").getAsString());
				Resource res = new Resource();
			//	res.setUrl(uploader.upload(jres.get("data").getAsByte()));
				ResourceDAO rdao = context.getBean(ResourceDAO.class);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/

		ps.setUser(user);
		ps.setValid(false);

		//aggiunge la categoria
		CategoryDAO cdao = SpringApplicationContext.getBean(CategoryDAO.class);
		ps.setCategory(cdao.find(jo.get("category_id").getAsInt()));

		//salva il suggest
		PoiSuggestionDAO pdao = SpringApplicationContext.getBean(PoiSuggestionDAO.class);
		pdao.save(ps);

		//ritorna conferma
		ConfirmResponse cr = new ConfirmResponse();
		cr.setMessage("OK");
		cr.setStatus(200);
		cr.setResult(ps.getId() + "");

		String ret = cr.serialize();
		System.out.println("responding: " + ret);
		return ret;
		
} catch(Exception ex) {
	
	//ritorna conferma
			ConfirmResponse cr = new ConfirmResponse();
			cr.setMessage(ex.getMessage());
			cr.setStatus(500);

			String ret = cr.serialize();
			System.out.println("responding: " + ret);
			return ret;
	
}


	}


	/**
	 * @see IWS#upload(int, String, byte[])
	 */
	public String upload(int poi_id, String type, byte[] data) {

		ConfirmResponse cr = new ConfirmResponse();
		AbstractResourceUploader uploader;
		try {
			uploader = ResourceUploaderFactory.create(type);
			Resource res = new Resource();
			String url = uploader.upload(data);
			res.setUrl(url);
			ResourceDAO rdao = SpringApplicationContext.getBean(ResourceDAO.class);
			PoiDAO pdao = SpringApplicationContext.getBean(PoiDAO.class);
			res.setPoi(pdao.find(poi_id));
			rdao.save(res);
			
			cr.setMessage("OK");
			cr.setStatus(200);
			cr.setResult(url);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			cr.setMessage(e.getMessage());
			cr.setStatus(500);
			cr.setResult(""); 
			
		}
		
		
		String ret = cr.serialize();
		System.out.println("responding: " + ret);
		return ret;
		
	}


}
