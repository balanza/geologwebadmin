package it.geolog.web.ws;

import it.geolog.web.domain.Poi;
import it.geolog.web.ws.types.PoiListResponse;

import java.util.List;



public interface IWS {

	
	/**
	 * metodo che espone la lista delle categorie attive nel sistema
	 * @return
	 */
	public String listCategories();


	/**
	 * Esegue una ricerca dei poi per vicinanza
	 * @param latitude latitudine di riferimento
	 * @param longitude longitudine di riferimento
	 * @param categories lista di categorie
	 * @return un oggetto in formato JSON contente la lista di poi 
	 */
	public String findNearby(double latitude, double longitude, String categories);
	
	
	/**
	 * registra una segnalazione 
	 * @param poi_id id del poi su cui effettuare la segnalazione
	 * @param msg messaggio
	 * @param user utente
	 * @return un oggetto JSON con la conferma di ricezione del messaggio
	 */
	public String reportPoi(int poi_id, String msg, String user);
	
	
	/**
	 * aggiunge un poi alla base dati del servizio
	 * @param poi poi da aggiungere
	 * @param user utente
	 * @return un oggetto JSON con la conferma di ricezione del messaggio
	 */
	public String addPoi(String poi, String user);
	
	
	/**
	 * Carica uan risorsa riferita ad un poi
	 * @param poi_id id del poi
	 * @param type tipo della risorsa
	 * @param data risorsa, come array di byte
	 * @return un oggetto JSON con la conferma di ricezione del messaggio
	 */
	public String upload(int poi_id, String type, byte[] data);
	
}
