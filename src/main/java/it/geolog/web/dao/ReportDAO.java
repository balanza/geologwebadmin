package it.geolog.web.dao;

import it.geolog.web.domain.Category;
import it.geolog.web.domain.Report;

import java.util.List;
import java.util.Map;
import java.util.Set;


import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class ReportDAO extends HibernateDaoSupport implements IDAO<Report> {

	
	
	@Override
	public long countAll(Map<String, Object> params) {
		// TODO Auto-generated method stub
		
		return 0;
	}

	@Override
	public void save(Report o) {
		getHibernateTemplate().saveOrUpdate(o);
	}

	@Override
	public void delete(int id) {
		getHibernateTemplate().delete(find(id));
	}
	
	@Override
	public void delete(Report o) {
		getHibernateTemplate().delete(o);
	}

	@Override
	public Report find(int id) {
		HibernateTemplate t = getHibernateTemplate();
		return (Report) t.get(Report.class, id);
	} 



	@SuppressWarnings("unchecked")
	@Override
	public List<Report> findAll() {
		return getHibernateTemplate().find( "from " + Report.class.getName());
	}
	
	public List<Category> findAll(int[] ids) {
		String s = "";
		for(int i: ids){
			s += i + ",";
		}
		s += "-1";
		
		return getHibernateTemplate().find("from " + Report.class.getName() + " where it.geolog.domain.Report.id in (" + s + ")" );
	}
	

	

	
}
