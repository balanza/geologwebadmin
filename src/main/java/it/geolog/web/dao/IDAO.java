package it.geolog.web.dao;


import it.geolog.web.domain.Category;

import java.util.List;
import java.util.Map;
import java.util.Set;


public interface IDAO<T> {
    /**
     * Method that returns the number of entries from a table that meet some
     * criteria (where clause params)
     *
     * @param params
     *            sql parameters
     * @return the number of records meeting the criteria
     */
    long countAll(Map<String, Object> params);

    void delete(int id);
    
    void delete(T o);

    T find(int id);
    
    List<T> findAll();

    void save(T o);   
}