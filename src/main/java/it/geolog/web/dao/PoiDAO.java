package it.geolog.web.dao;

import it.geolog.web.domain.Category;
import it.geolog.web.domain.Poi;

import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.List;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.web.context.ContextLoaderListener;


public class PoiDAO extends BaseDAO<Poi> {
	
	/**
	 * come esegue una ricerca di record a seconda dei filtri proposti
	 * @return
	 */
	public List<Poi> search(String key, int categoryId, int approvation) {

		String where = "where 1=1";
		
		if(categoryId != 0){
			where += " and category.id = " + categoryId +  "";			
		}

		if(approvation == 0){
			where += " and valid = 0";			
		} else if(approvation == 1){
			where += " and valid = 1";			
		} 

		if(!key.equals("")){
			where += " and (name like '%" + key +  "%' or description  like '%" + key +  "%') ";			
		}
		
		return (List<Poi>)getHibernateTemplate().find("from Poi P " + where);
		
		
	}
	
	
	/**
	 * come findAll, ma permette di specificare se si vogliono solamente poi con il flag di validazione attivo
	 * @see BaseDAO#findAll()
	 * @param onlyValid se vero, filtra i risultati omettendo i poi con flag di validazione negativo
	 * @return
	 */
	public List<Poi> findAll(Boolean onlyValid) {

		if(onlyValid){
			return getHibernateTemplate().find("from Poi P where P.valid = 1");
		} else {
			return findAll();
		}
	}
	

	/**
	 * come findAll, ma filtra i risultati per un elenco di categorie e li ordina per distanza crescente dalle coordinate specificate
	 * @see BaseDAO#findAll()
	 * @param lat latitudine delle coordinate di riferimento
	 * @param lon longitudine delle coordinate di riferimento
	 * @param categories lista dell categorie a cui devono appartenere i poi
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Poi> findAll(double lat, double lon, List<Category> categories) {
		return findAll(lat, lon, categories, true);
	}
	
	public List<Poi> findAll(double lat, double lon, List<Category> categories, Boolean onlyValid) {
		String s = "";
		for(Category c : categories){
			s += c.getId() + ",";
		}
	
		s += "-1";
		
		
		
		String distFormula = String.format("6371000 * ACOS(SIN(RADIANS( %f )) * SIN(RADIANS( lat )) + COS(RADIANS( %f )) * COS(RADIANS( lat )) * COS(RADIANS( lon ) - RADIANS( %f )))", lat, lat, lon);
	
		if(onlyValid){
			return getHibernateTemplate().find( "from Poi P where P.valid = 1 and category.id in (" + s + ") order by (" + distFormula + ")");
		} else {
			return getHibernateTemplate().find("from Poi P order by (" + distFormula + ")");
		}
		
	}

}
