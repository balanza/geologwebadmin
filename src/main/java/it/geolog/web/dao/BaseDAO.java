package it.geolog.web.dao;

import it.geolog.web.domain.Category;

import java.lang.reflect.*;
import java.util.List;
import java.util.Map;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public abstract class BaseDAO<T extends it.geolog.web.domain.AbstractEntity> extends HibernateDaoSupport implements IDAO<T> {

	private Class<T> type; //serve per mantenere un riferimento del tipo di DAO
	protected String clazz; 
	
	public BaseDAO() {
		// http://www.codeproject.com/Articles/251166/The-Generic-DAO-pattern-in-Java-with-Spring-3-and
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		this.type = (Class) pt.getActualTypeArguments()[0];
		
	}
	
	public BaseDAO(String clazz) {
		this.clazz = clazz;
	}


	/**
	 * ritorna il numero totale di record per il tipo richiesto
	 * @return il numero di record totali
	 */
	
	public long countAll(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * cancella un record dalla base di dati 
	 * @param id identificativo del record 
	 */
	
	public void delete(int id) {
		delete(find(id));
	}

	/**
	 * cancella un record dalla base di dati
	 * @param o l'oggetto che mappa il record da eliminare
	 */
	
	public void delete(T o) {
		getHibernateTemplate().delete(o);		
	}

	/**
	 * cerca un record nella base di dati a partire dall'identificativo
	 * @param id identificativo del record
	 * @return l'oggetto che mappa il record trovato, null se non trova nulla
	 */
	@SuppressWarnings("unchecked")

	

	/**
	 * se l'oggetto mappa un nuovo record, lo crea nella basae di dati, altrimenti salva le modifiche all'oggetto riferito
	 * @param o l'oggetto da salvare
	 */

	public void save(T o) {
		// TODO Auto-generated method stub
		getHibernateTemplate().save(o);
	}
	
	
	public T find(int id) {
		try{
			String ntt =  type.getName(); //specific entity to search into
			return (T) getHibernateTemplate().find( "from " + ntt + " where id = " + id).get(0);	
		} catch(Exception e){
			return null;
			
		}
		
	}


	/**
	 * lista di tutti i record della base di dati
	 * @return la lista dei record per il tipo richiesto
	 */
	
	public List<T> findAll() {
		String ntt =    type.getName();
		return (List<T>) getHibernateTemplate().find( "from " + ntt);
	}

}
