package it.geolog.web.dao;

import it.geolog.web.domain.Category;

import java.util.List;
import java.util.Map;
import java.util.Set;


import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class CategoryDAO extends BaseDAO<Category> {

	
	public CategoryDAO(){
		super();		
		this.clazz = "Category";
	}
	
	
	
	/**
	 * ritorna la lista di categorie i cui identificativi sono contenuti nell'array di input	
	 * @param ids elenco di identificativi
	 * @return lista di categorie
	 */
	public List<Category> findAll(int[] ids) {
		String s = "";
		for(int i: ids){
			s += i + ",";
		}
		s += "-1";
		
		return getHibernateTemplate().find("from " + Category.class.getName() + " where id in (" + s + ")" );
	}
	
	
	public Category find(int id) {
		String ntt = this.clazz;// type.getClass().getName(); //specific entity to search into
		return (Category) getHibernateTemplate().find( "from " + ntt + " where id = " + id).get(0);
	}


	/**
	 * lista di tutti i record della base di dati
	 * @return la lista dei record per il tipo richiesto
	 */
	
	public List<Category> findAll() {
		String ntt =  this.clazz; //   type.getClass().getName(); //specific entity to search into
		return (List<Category>) getHibernateTemplate().find( "from " + ntt);
	}
	
	

	

	
}
