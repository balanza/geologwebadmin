package it.geolog.web.domain;

public class Report extends AbstractEntity {

	private Poi poi; //poi a cui si riferisce la segnalazione
	private String message; //messaggio della segnalazione
	private String user; //utente segnalatore

	/**
	 * metodo getter per il poi
	 * @return poi a cui si riferisce la segnalazione
	 */
	public Poi getPoi() {
		return poi;
	}

	/**
	 * metodo setter per il poi
	 * @return poi a cui si riferisce la segnalazione
	 */
	public void setPoi(Poi poi) {
		this.poi = poi;
	}

	/**
	 * metodo getter per il messaggio della segnalazione
	 * @return messaggio della segnalazione
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * metodo setter per il messaggio della segnalazione
	 * @param message messaggio della segnalazione
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	

	/**
	 * metodo getter per l'utente segnalatore
	 * @return utente segnalatore
	 */
	public String getUser() {
		return user;
	}

	/**
	 * metodo setter per l'utente segnalatore
	 * @param user utente segnalatore
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @see AbstractEntity#getLabel()
	 */
	@Override
	public String getLabel() {
		return "";
	}
}
