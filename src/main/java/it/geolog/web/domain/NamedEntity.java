package it.geolog.web.domain;

/**
 * @author Emanuele De Cupis
 *
 */
public abstract class NamedEntity extends AbstractEntity {


	private String name; //nome letterale dell'oggetto
	private String description; //descrizione testuale dell'oggetto

	
	
	/**
	 * Metodo getter per il nome letterale
	 * @return il nome letterale
	 */
	public String getName() {
		return name;
	}


	/**
	 * Metodo setter per il nome letterale
	 * @param name il nome letterale
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * Metodo getter per la descrizione testuale
	 * @return descrizione testuale
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * Metodo setter per la descrizione testuale
	 * @param description descrizione testuale
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	
	
	
	/** 
	 * @see it.geolog.domain.IEntity#getLabel()
	 */
	@Override
	public String getLabel() {
		return this.getName();
	}

	

}
