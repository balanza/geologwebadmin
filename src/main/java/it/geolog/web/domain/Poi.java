package it.geolog.web.domain;

import java.util.Date;
import java.util.Set;



public class Poi extends NamedEntity{
	
	private Category category; //categoria del poi
	private float latitude; //latitudine del poi
	private float longitude; //longitudine del poi
	private Set<Resource> resources; //insieme delle risorse del poi
	private Date creationDate; //data di creazione del poi
	private Boolean valid; //vero se il poi è valido, falso viceversa
	private Set<Report> reports; //insieme dei report del poi
	
	
	
	/**
	 * Metodo getter per l'insieme di report associati al poi
	 * @return insieme dei report associati al poi
	 */
	public Set<Report> getReports() {
		return reports;
	}


	/**
	 * Metodo setter per l'insieme di report associati al poi
	 * @param reports insieme dei report associati al poi
	 */
	public void setReports(Set<Report> reports) {
		this.reports = reports;
	}


	
	/**
	 * Metodo getter per il flag di validità del poi
	 * @return valore del flag di validità del poi
	 */
	public Boolean getValid() {
		return valid;
	}


	/**
	 * Metodo setter per il flag di validità del poi
	 * @param valid valore del flag di validità del poi
	 */
	public void setValid(Boolean valid) {
		this.valid = valid;
	}


	

	/**
	 * Metodo getter per l'insieme di risorse associate al poi
	 * @return insieme di risorse associate al poi
	 */
	public Set<Resource> getResources() {
		return resources;
	}


	/**
	 * Metodo setter per l'insieme di risorse associate al poi
	 * @param resources insieme di risorse associate al poi
	 */
	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}


	
	/**
	 * Metodo getter per la data di creazione del poi
	 * @return data di creazione 
	 */
	public Date getCreationDate() {
		return creationDate;
	}


	/**
	 * Metodo setter per la data di creazione del poi
	 * @param creationDate data di creazione 
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	public Poi(){}
	
	

	

	/**
	 * Metodo getter per la latitudine del poi
	 * @return latitudine 
	 */
	public float getLatitude(){
		return this.latitude;
	}


	/**
	 * Metodo setter per la latitudine del poi
	 * @param latitude latitudine 
	 */
	public void setLatitude(float latitude){
		this.latitude = latitude;
	}
	

	/**
	 * Metodo getter per la longitudine del poi
	 * @return longitude 
	 */
	public float getLongitude(){
		return this.longitude;
	}
	

	/**
	 * Metodo setter per la longitudine del poi
	 * @param longitude longitudine 
	 */
	public void setLongitude(float longitude){
		this.longitude = longitude;
	}

	
	/**
	 * Metodo getter per la categoria del poi
	 * @return categoria del poi 
	 */
	public Category getCategory(){
		return category;
	}

	/**
	 * Metodo setter per la categoria del poi
	 * @param category categoria del poi 
	 */
	public void setCategory(Category category){
		this.category = category;
	}
	

}
