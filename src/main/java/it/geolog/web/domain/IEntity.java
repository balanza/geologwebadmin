package it.geolog.web.domain;


public interface IEntity extends Comparable<IEntity> {

	public int getId();
	public void setId(int id);
	
	/**
	 * Testo riconoscitivo usato per visualizzare l'entità
	 * @return un etichetta riconoscitiva
	 */
	public String getLabel();
	
}