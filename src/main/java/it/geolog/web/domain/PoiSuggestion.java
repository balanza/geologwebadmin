package it.geolog.web.domain;

public class PoiSuggestion extends Poi {
	
	private String user; //stringa che identifica l'utente che ha effettuato la segnalazione
	
	/**
	 * metodo getter per l'utente segnalatore
	 * @return l'utente segnalatore
	 */
	public String getUser() {
		return user;
	}
	/**
	 * metodo setter per l'utente segnalatore
	 * @param user l'utente segnalatore
	 */	
	public void setUser(String user) {
		this.user = user;
	}
}
