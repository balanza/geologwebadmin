package it.geolog.web.domain;


public class Resource extends AbstractEntity{
	
	private String url; //url della risorsa		
	private String description; //descrizione della risorsa
	private Poi poi; //poi a cui � associata la risorsa
	private String resourceType; //tipo della risorsa
	
	
	public Resource(){}
	
	/**
	 * metodo getter per la url della risorsa
	 * @return url della risorsa
	 */
	public String getUrl(){
		return this.url;
	}

	/**
	 * metodo setter per la url della risorsa
	 * @param url url della risorsa
	 */
	public void setUrl(String url){
		this.url = url;
	}
	

	/**
	 * metodo getter per la descrizione della risorsa
	 * @return descrizione della risorsa
	 */
	public String getDescription(){
		return this.description;
	}

	/**
	 * metodo setter per la descrizione della risorsa
	 * @param description descrizione della risorsa
	 */
	public void setDescription(String description){
		this.description = description;
	}
	

	/**
	 * metodo getter per il poi a cui � associata la risorsa 
	 * @return il poi a cui � associata la risorsa 
	 */
	public Poi getPoi(){
		return this.poi;
	}

	/**
	 * metodo setter per il poi a cui � associata la risorsa 
	 * @param poi il poi a cui � associata la risorsa 
	 */
	public void setPoi(Poi poi){
		this.poi = poi;
	}	

	/**
	 * metodo getter per il tipo della risorsa
	 * @return il tipo della risorsa 
	 */	
	public String getResourceType(){
		return this.resourceType;
	}

	/**
	 * metodo setter per il tipo della risorsa
	 * @param type il tipo della risorsa 
	 */	
	public void setResourceType(String type){
		this.resourceType = type;
	}

	/**
	 * @see AbstractEntity#getLabel()
	 */
	@Override
	public String getLabel() {
		return getUrl();
	}
	
	
}
