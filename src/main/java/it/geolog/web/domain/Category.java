package it.geolog.web.domain;

import java.util.Set;

public class Category extends NamedEntity {
	
	private String icon; //url dell'icona associata alla categoria
	private Set<Poi> pois; //insieme dei poi appartenenti alla categoria
	
	public Category(){}
	
	/**
	 * Metodo getter per la url dell'icona
	 * @return url dell'icona
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * Metodo setter per la url dell'icona
	 * @param icon url dell'icona
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
		

	/**
	 * Metodo getter per l'insieme di poi associati
	 * @return insieme di poi associati
	 */
	public Set<Poi> getPois(){
		return this.pois;
	}

	/**
	 * Metodo setter per l'insieme di poi associati
	 * @param pois insieme di poi associati
	 */
	public void setPois(Set<Poi> pois){
		this.pois = pois;
	}

	
}
