package it.geolog.web.domain;

public abstract  class AbstractEntity implements IEntity{
	
	private int id; //identificativo numerico associato all'entità

	
	/**
	 * Comprara due entità in base all'id
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(IEntity o) {
		// TODO Auto-generated method stub
		if(o.getId() == this.getId()){
			return 0;
		} else if(o.getId() > this.getId()){
			return 1;
		} else {
			return -1;
		}
	}

	/**
	 * Metodo getter per l'identificativo univoco dell'entità
	 * @return numero identificativo univoco dell'entità
	 * @see it.geolog.domain.IEntity#getId()
	 */
	@Override
	public int getId() {
		return this.id;
	}


	/**
	 * Metodo setter per l'identificativo univoco dell'entità
	 * @param numero identificativo univoco dell'entità
	 * @see it.geolog.domain.IEntity#setId()
	 */
	@Override
	public void setId(int id) {
		this.id = id;
	}

	

}
